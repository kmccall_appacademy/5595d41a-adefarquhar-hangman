require 'byebug'
class Hangman
  attr_reader :guesser, :referee, :board

  # figure out defaults for players option hash
  # want to create options to choose who is the guesser
  # and who is the referee, whether computer or human
  def initialize(players = { guesser: "ComputerPlayer", referee: "ComputerPlayer" })
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    secret_len = referee.pick_secret_word
    guesser.register_secret_length(secret_len)
    @board = Array.new(secret_len)
  end

  def take_turn
    guess = guesser.guess
    ltr_pos = referee.check_guess(guess)
    update_board(ltr_pos, guess) unless ltr_pos.empty?
    guesser.handle_response
  end

  def update_board(ltr_pos, ltr)
    ltr_pos.each { |pos| @board[pos] = ltr }
  end

end

class HumanPlayer
end

class ComputerPlayer
  attr_reader :dictionary, :secret_word, :candidate_words

  def initialize(dictionary = File.readlines("dictionary.txt"))
    @dictionary = dictionary
    #@secret_word = dictionary.sample
  end

  def pick_secret_word
    # implement way to receive random word from dictionary file
    @secret_word = dictionary.sample
    secret_word.length
  end

  def register_secret_length(length)
    words_left = dictionary.select do |wrd|
      wrd if wrd.length == length
    end

    @candidate_words = words_left
  end

  # def candidate_words
  #   register_secret_length(pick_secret_word)
  #   # words
  # end

  def guess(board)
    guessed = ''
    board.each { |ltr| guessed << ltr unless ltr.nil? }
    letters = candidate_words.join.delete(guessed)

    most_common_letter(letters)
  end

  def most_common_letter(letters)
    ltr_count = Hash.new(0)
    letters = letters.chars
    letters.each { |ltr| ltr_count[ltr] += 1 }
    max = ltr_count.values.max

    ltr_count.key(max)
  end

  def check_guess(ltr)
    ltr_idx = []
    letters = secret_word.chars
    letters.each_index do |idx|
      ltr_idx << idx if letters[idx] == ltr
    end

    ltr_idx
  end

  def handle_response(ltr, idxs)
    words = candidate_words
    ltr_frq = idxs.size

    if idxs.empty?
      words.select! { |wrd| wrd unless wrd.include?(ltr) }
    end

    idxs.each do |idx|
      words.select! { |wrd| wrd if wrd[idxs.first] == ltr }
      words.select! { |wrd| wrd if wrd.count(ltr) == ltr_frq }
    end

    @candidate_words = words
  end
end
